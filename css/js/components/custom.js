$(function() {


    if ($('#body_design')) {
        var backLink = $('.crumb-mobile');

        $('#crumbs').append(backLink);
    }

    $('#crumbs a[href="/"]').html('<i class="icon icon-home"></i>');


    if ($(window).width() < 991) {
        $('#navbar-basket').text(function(index, text) {
            return text.replace('items', '');
        });
    }

    // Open and clode sidebar main menu items

   $('#sidebar .widget_content ul.list > li:first-child > a').each(function(){
        $(this).parent('li').css({'maxHeight': ($(this).parent('li').children('ul').height()+55) + 'px'});
        $(this).parent('li').addClass('active');
    });

    $('#sidebar .widget_content').on('click', '.list.list-vertical > li > a', function(e){
        console.log(e);
        e.preventDefault();

        $(this).parent('li').siblings('li').each(function(){
            $(this).removeClass('active');
            $(this).css({'maxHeight': '55px'});
        });


        if($(this).parent('li').hasClass('active')){
            $(this).parent('li').css({'maxHeight': '55px'});
        } else {
            $(this).parent('li').css({'maxHeight': ($(this).parent('li').children('ul').height()+55) + 'px'});

        }
        $(this).parent('li').toggleClass('active');
    });

    // close and open filter

    $('#filter_cards').prepend('<h3 class="filtertop">Filter op</h3>');
    $('h3.filtertop').on('click', function() {
        $('#filter_menu').slideToggle();
        $(this).toggleClass('close');
    });

    $('.envmenu h3 > a').on('click', function(){

        $(this).toggleClass('close');
        $(this).parent().siblings('ul').slideToggle();
    });

});