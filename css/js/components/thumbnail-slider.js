$(function () {
    var $thumbnailSlider = $('.thumbnail-slider');

    $thumbnailSlider.each(function () {
        var _self = $(this);
        var middleCenter = _self.find('.thumbnail-slider-middle img');
        _self.find('.thumbnail-slider-right').hide();
        _self.find('.thumbnail-slider-left').hide();

        if (middleCenter.length > 1) {
            _self.find('.thumbnail-slider-right').show();
            _self.find('.thumbnail-slider-left').show();
            _self.find('.thumbnail-slider-middle').cycle();
        }
    });

    $thumbnailSlider.find('.thumbnail-slider-right a').click(function (e) {
        e.preventDefault();
        var _self = $(this);
        var target = _self.closest($thumbnailSlider).find('.thumbnail-slider-middle');
        target.cycle('next');
    });

    $thumbnailSlider.find('.thumbnail-slider-left a').click(function (e) {
        e.preventDefault();
        var _self = $(this);
        var target = _self.closest($thumbnailSlider).find('.thumbnail-slider-middle');
        target.cycle('prev');
    });

});