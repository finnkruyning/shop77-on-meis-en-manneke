$(function() {
    var $navbar = $('.navbar');

    if (!$("#headerlogin").length) {
        $('.topbar-menu-right').find('#navbar-login').remove();
    }
    if (!$("#headerlogout").length) {
        $('.topbar-menu-right').find('#navbar-logout').remove();
    }

    $navbar.find('.open-navbar-search').on('click', function(e) {
        e.preventDefault();
        e.stopImmediatePropagation();
        var _self = $(this);
        _self.toggle();
        var target = _self.closest($navbar).find('.navbar-search-field');
        target.toggle();
    });

    $navbar.find('.navbar-hamburger').on('click', function (e) {

        e.preventDefault();
        var _self = $(this);
        var target = _self.closest($navbar).find('.collapse');
        $('.navbar-hamburger').toggleClass('open');
        target.slideToggle(400, function () {
            if (target.is(":visible")) {
                $('.navbar-hamburger').addClass('open');
                $('html, body').css('overflowY', 'hidden');
            } else {
                $('.navbar-hamburger').removeClass('open');
                $('html, body').css('overflowY', 'auto');
            }
        });
    });

    if ($('.topbar-account-mob').length) {
        $('.topbar-account-mob').click(function() {
            console.log('eddd');
            $('.topbar-basket-menu-mob').hide();
            $('.topbar-account-menu-mob').toggle('slow');
        })
    }

    if ($('.topbar-basket-mob').length) {
        $('.topbar-basket-mob').click(function() {
            $('.topbar-basket-menu-mob').toggle('slow');
            $('.topbar-account-menu-mob').hide();
        });
    };


    $navbar.find('.js-dropdown-hover').on('mouseenter', function(e) {
        e.preventDefault();
        var _self = $(this);
        _self.closest('.js-dropdown').toggleClass('active');
        var target = _self.closest('.js-dropdown').find('.js-dropdown-content');
        target.toggle();
    });

    $navbar.find('.js-dropdown').on('mouseleave', function(e) {
        e.preventDefault();
        var _self = $(this);
        _self.closest('.js-dropdown').removeClass('active');
        var target = _self.closest('.js-dropdown').find('.js-dropdown-content');
        target.hide();
    });

    $navbar.find('.js-dropdown-click').on('click', function(e) {
        e.preventDefault();
        var _self = $(this);
        _self.closest('.js-dropdown').toggleClass('active');
        var target = _self.closest('.js-dropdown').find('.js-dropdown-content');
        target.toggle();
    });

    $navbar.find('.js-dropdown').on('mouseleave', function(e) {
        e.preventDefault();
        var _self = $(this);
        _self.closest('.js-dropdown').removeClass('active');
        var target = _self.closest('.js-dropdown').find('.js-dropdown-content');
        target.hide();
    });

    if ($(window).width() < 991) {
        $('.basket-count-mob').text(function(index, text) {
            return text.replace(' items', '');
        });
    };

    if ($(window).width() > 992) {
        $(window).scroll(function() {
            if ($(document).scrollTop() > 75) {
                $('.dropdown-content').hide();
            }
        });
    };

    if ($(document).width() < 767) {
        $(window).scroll(function() {
            if ($(document).scrollTop() > 50) {
                $('.navbar .middlebar').hide();
                $('.navbar .topbar').addClass('topbar-border');
                $('.navbar').addClass('navbar-collapsed');
            } else {
                $('.navbar .middlebar').show();
                $('.navbar .topbar').removeClass('topbar-border');
                $('.navbar').removeClass('navbar-collapsed');
            }
        });
    };

    $(window).load(function() {

        var $header = $('#header');
        var $headerMenu = $header.find('.topbar-menu-right');
        var $topbarBasketMob = $header.find('.topbar-basket-mob');
        var currentCountHtml = $("#headerbasket #basket_count").html();
        var currentCount = currentCountHtml.replace('items', '');
        currentCount = currentCount.replace('item', '');


        if ($('.topbar-basket-mob').length) {
            if ($('#navbar-basket').text().trim() == '0 items') {
                $('.topbar-basket-menu-mob .basket-items-count').text('Je winkelmandje is leeg');
            } else if ($('#navbar-basket').text().trim().substring(0, 2) == '1 ') {
                $('.topbar-basket-menu-mob .basket-items-count').text('1 item in je winkelmandje');
            } else {
                $('.topbar-basket-menu-mob .basket-items-count').text($('#navbar-basket').text() + 'in je winkelmandje');
            }
        }
    });




});



