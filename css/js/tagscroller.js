
$(function() {

    function setHome() {
        if ($('.mainCont').length) {
            $('.mainCont').append('<div class="sliderBlock"><div id="pSlide"></div><div id="nSlide"></div><div class="slideWrap"></div></div><div class="topCards"></div>');
            $('#card_previews a').each(function(i) {
                var $u = $(this).find('img').attr('real-src');
                //$(this).find('img').attr('src',$u);
                if ($('#headerlogin').length > 0) {
                    if (i < 10) {
                        $(this).not('.trash_icon, .add_user_design').appendTo('.slideWrap');
                    } else {
                        $(this).appendTo('.topCards');
                    }
                } else {
                    if (i < 10) {
                        $(this).not('.trash_icon, .add_user_design').find('img').attr('src', $u).parent().appendTo('.slideWrap');
                    } else {
                        $(this).not('.trash_icon, .add_user_design').find('img').attr('src', $u).parent().appendTo('.topCards');
                    }
                }
            })

            $('#card_previews').remove();
        } else {
            console.log('Er is geen mainCont div aanwezig');
        }
    }

    $(window).on('load', function() {
        setHome();
        if ($('.sliderBlock').length > 0) {
            $('div.slideWrap').cycle({
                timeout: 0,
                slides: '> a',
                prev: '#pSlide',
                next: '#nSlide',
                fx: 'carousel',
                allowWrap: true,
                speed: 500,
                swipe: true,
                swipeFx: 'carousel'
            });
        }
    });
});